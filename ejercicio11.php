<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="estilos.css"/>
    </head>
    <body>
        <div>
            <h1>Formulario de inscripción de usuarios</h1>
            <form name="f">
            <table border="1" cellspacing="0">
                <tbody>
                    <tr>
                        <td><p>Nombre Completo</p></td>
                        <td class="formu">
                            <input type="text" name="nombre" id="nombre" />
                        </td>
                    </tr>
                    <tr>
                        <td><p>Dirección</p></td>
                        <td class="formu">
                            <textarea id="dire" name="dire" rows="6" cols="40"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><p>Correo electronico</p></td>
                        <td class="formu">
                            <input type="password" name="contra" id="contra">
                        </td>
                    </tr>
                    <tr>
                        <td><p>Confirmar contraseña</p></td>
                        <td class="formu">
                            <input type="password" name="contraV" id="contraV">
                        </td>                                                
                    </tr>
                    <tr>
                        <td><p>Fecha de Nacimiento</p></td>
                        <td class="formu">
                            <input type="date" name="fecha" id="fecha">
                        </td>
                    </tr>
                    <tr>
                        <td><p>Sexo</p></td>
                        <td class="formu">
                            <input type="radio" name="sexo" id="H">Hombre<br>
                            <input type="radio" name="sexo" id="M">Mujer<br>
                        </td>
                    </tr>
                    <tr>
                        <td><p>Por favor elige los temas de tus intereses</p></td>
                        <td class="formu">
                            <input type="checkbox" name="gustos[]" value="Ficcion">Ficcion<br>
                            <input type="checkbox" name="gustos[]" value="Accion">Accion<br>
                            <input type="checkbox" name="gustos[]" value="Suspense">Suspense<br>
                            <input type="checkbox" name="gustos[]" value="Terror">Terror<br>
                            <input type="checkbox" name="gustos[]" value="Comedia">Comedia<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                            Selecciona tus aficiones
                            (Selecciona multiples elementos pulsando
                            la tecla Control
                            y haciendo clic en cada uno, uno a uno)
                            </p>
                        </td>
                        <td class="formu">
                            <select class="selector" multiple name="aficiones[]" >
                                <option value="0">Deportes al aire libre</option>
                                <option value="1">Deportes de aventura</option>
                                <option value="2">Música Pop</option>
                                <option value="2">Música Rock</option>
                                <option value="2">Música alternativa</option>
                                <option value="2">Fotografia</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
                <input type="submit" value="Enviar" name="boton" />
            </form>
        </div>
        <?php
            if(isset($_GET["boton"])){
                var_dump($_GET);
            }
        ?>
    </body>
</html>
