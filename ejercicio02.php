<?php
if(empty($_REQUEST)){
    $mal = true;
    $error = "Introducir los datos en el formulario"; // mensaje
}elseif (empty ($_REQUEST["numero"])) {
    $mal=true;
    $error="El numero es obligatorio"; // mensaje que sale en caso de que no metas datos
}elseif ($_REQUEST["numero"] < 0) {
    $mal= true;
    $error = "El numero introducido debe ser mayor que 0"; // mensaje que sale en caso de que el dato sea menor de 0
}else{
    $mal = false;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if(!$mal){
            var_dump($_REQUEST); // comprobacion
        } else { // en caso de que no meter datos recarga el formulario
            echo $error;       
        ?>
        <div>
            <form name="f">
                Numero<input type="number" name="numero" />
                <input type="submit" value="Enviar" name="boton" />
            </form>
        </div>
        <?php
        }
        ?>
        
    </body>
</html>
